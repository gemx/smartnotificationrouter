# WearableCompanion

## What's it for?
It's main purpose is to bring bb notifications such as incoming call or mails to <br/>
android applications that also run on the BB device. <br/>
It also supports direct notifications to sony smartwatches (SW1 and SW2).

## Current Version 
Beta 1
Beta 2 
Beta 3 
Beta 4 (just a bugfix release)
**Beta 4-ext (trial period extended to 05/31/2015**

## Feature Status
* **Create Android wear compatible android notifications for**
    * Incoming Mails (Beta 1)
    * Incoming Textmessages (Beta 2)
    * Incoming Calls (Beta 1)
    * Pushbullet notes (Beta 2)
    * Calendar events (Beta 3)
    * BBM messages (Beta 3)
* **Create Sony smartwatch notifications for**
    * Incoming Mails (Beta 1)
    * Incoming Textmessages (Beta 2)
    * Incoming Calls (Beta 1)
    * Pushbullet notes (Beta 2)
    * Notifications created by other android apps (Beta 2)
    * Calendar events (Beta 3)
    * BBM messages (Beta 3)

**!!!ATTENTION!!!** <br>
I can only detect if there was any kind of activity in your BBM account.<br/>
So you get a notification even if you post a message yourself.
Sorry for that but this is a limitation of the api :-(

## Install

The solution consists of 2 parts. A blackberry app in form of a .bar file and the android application in form of an apk file

1) **The native blackberry app** <br/>
Please follow the instructions here http://forums.berryverse.com/threads/76-How-to-Side-Load-apps-on-your-BlackBerry-10-device for the installation itself.<br/>
The application is automatically started after installing. <br/>
The UI app is just for testing a notification<br/>

2) **The android app** <br/>
Just copy the apk file to your device and execute it via the file manager. <br/>
You should start the application once and go to settings (the three dotted line on top right corner). <br/>
If you use a sony smartwatch then you should click "Blacklisted apps". <br/>
If clicked for the first time this will send you to the "Notification Access" screen.<br/>
You should check the checkbox for "NotificationDelegator" to allow it to grab notifications from other app.<br/>
**If it doesn't catch any notification you should restart your phone.**<br/>
Sorry for that but that happens sometimes due to an Android bug<br/>

## Issue Reporting and feature requests
Please use the **Issues** tab to file and track issues.